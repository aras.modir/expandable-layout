package com.example.expandablelayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

public class MainActivity extends AppCompatActivity {
    Button button;
    ExpandableRelativeLayout relativeLayout;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        relativeLayout = findViewById(R.id.mycontent);
        button = findViewById(R.id.button);
        textView = findViewById(R.id.text);
        relativeLayout.toggle();
    }

    public void showMyInformation(View view) {
        relativeLayout.toggle();

    }
}
